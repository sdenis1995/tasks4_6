import mincemeat
import string
import sys

data = ["Humpty Dumpty sat on a wall",
        "Humpty Dumpty had a great fall",
        "All the King's horses and all the King's men",
        "Couldn't put Humpty together again",
        ]

translator = str.maketrans('', '', string.punctuation)

data = dict()
fp = open("./southpark/All-seasons.csv")
line_count = 0
found_prev = False
key_prev = ""
for line in fp.readlines():
    if not found_prev:
        spl = line.split(',')
        line_count+=1
        if len(spl) == 4:
            #print(spl[3].translate(translator).lower().strip())
            if spl[2] not in data:
                data.update({spl[2] : ""})
            data[spl[2]] += " " + spl[3].translate(translator).lower().strip()
            if spl[3].count('"') < 2:
                key_prev = spl[2]
                found_prev = True
    else:
        data[key_prev] += " " + line.translate(translator).lower().strip()
        if line.count('"') > 0:
            key_prev = ""
            found_prev = False

for key in data:
    data[key] = data[key].strip()
# The data source can be any dictionary-like object

def mapfn(k, v):
    for w in v.split():
        yield k, w

def reducefn(k, vs):
    result = len(set(vs))
    return result

s = mincemeat.Server()
s.datasource = data
s.mapfn = mapfn
s.reducefn = reducefn

results = s.run_server(password="123")
fp = open("./data/task4.csv", "w")
for key in results:
    fp.write("{},{}\n".format(key, results[key]))
