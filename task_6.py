#!/usr/bin/env python
import mincemeat
import os
import sys
import string
import re
import numpy as np

import csv
tmp_data = dict()
tmp_data.update({'a' : []})
tmp_data.update({'b' : []})

with open("./matrix/matrix.csv", "r") as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        tmp_data[row[0]].append([int(row[1]), int(row[2]), int(row[3])])

max_index = max([item[0] for item in tmp_data['a']]) + 1
data = dict()
data.update({'a' : np.zeros((max_index, max_index))})
data.update({'b' : np.zeros((max_index, max_index))})
for item in tmp_data['a']:
    data['a'][item[0], item[1]] = item[2]
for item in tmp_data['b']:
    data['b'][item[0], item[1]] = item[2]

def mapfn(k, v):
    if k == 'a':
        for i in range(len(v)):
            for j in range(len(v)):
                for l in range(len(v)):
                    yield (i, j), (l, v[i, l])
    if k == 'b':
        for i in range(len(v)):
            for j in range(len(v)):
                for l in range(len(v)):
                    yield (i, j), (l, v[l, j])

def reducefn(k, vs):
    import numpy as np
    max_val = max([item[0] for item in vs]) + 1
    result = sum([np.prod([item[1] for item in vs if item[0] == i]) % 97 for i in range(max_val)])
    result = result % 97
    return result

s = mincemeat.Server()
s.datasource = data
s.mapfn = mapfn
s.reducefn = reducefn

results = s.run_server(password="123")
#sys.exit(0)
fp = open("./data/task6.csv", "w")

for indices in results:
    fp.write("{},{},{}\n".format(indices[0], indices[1], results[indices]))
fp.close()