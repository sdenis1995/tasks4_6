#!/usr/bin/env python
import mincemeat
import os
import sys
import string
import re

filenames = os.listdir("./sherlock/")
filenames = [item.split('.')[0] for item in filenames]
data = dict()
translator = str.maketrans('', '', string.punctuation)
for item in filenames:
    fp = open("./sherlock/{}.txt".format(item))
    tmp = fp.read().translate(translator).lower().strip()
    tmp = " ".join(tmp.split())
    data.update({item : tmp})


def mapfn(k, v):
    for w in v.split():
        yield w, k

def reducefn(k, vs):
    from collections import Counter
    result = Counter(vs)
    return result

s = mincemeat.Server()
s.datasource = data
s.mapfn = mapfn
s.reducefn = reducefn

results = s.run_server(password="123")
fp = open("./data/task5.csv", "w")
fp.write("," + ",".join(filenames) + "\n")
for word in results:
    str = word
    for file in filenames:
        if file in results[word]:
            str += ",{}".format(results[word][file])
        else:
            str += ",0"
    fp.write(str + '\n')

#print(results)
